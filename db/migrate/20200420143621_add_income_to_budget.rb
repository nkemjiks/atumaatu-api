class AddIncomeToBudget < ActiveRecord::Migration[6.0]
  def change
    add_column :budgets, :income, :integer, default: 0
  end
end
