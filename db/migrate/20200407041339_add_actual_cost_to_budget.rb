class AddActualCostToBudget < ActiveRecord::Migration[6.0]
  def change
    add_column :budgets, :actual_cost, :integer, default: 0
    change_column_default :budgets, :budgeted_cost, default: 0
  end
end
