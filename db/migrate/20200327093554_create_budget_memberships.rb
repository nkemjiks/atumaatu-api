class CreateBudgetMemberships < ActiveRecord::Migration[6.0]
  def change
    create_table :budget_memberships do |t|
      t.integer :budget_id
      t.integer :item_id
      t.integer :cost

      t.timestamps
    end
  end
end
