class RenameCostToBudgetedOnBudgetMembership < ActiveRecord::Migration[6.0]
  def change
    rename_column :budget_memberships, :cost, :budgeted_cost
  end
end
