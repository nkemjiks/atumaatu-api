class RenameCostToBudgetedOnBudget < ActiveRecord::Migration[6.0]
  def change
    rename_column :budgets, :cost, :budgeted_cost
  end
end
