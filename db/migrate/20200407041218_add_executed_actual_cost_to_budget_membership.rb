class AddExecutedActualCostToBudgetMembership < ActiveRecord::Migration[6.0]
  def change
    add_column :budget_memberships, :executed, :boolean, default: false
    add_column :budget_memberships, :actual_cost, :integer, default: 0
    change_column_default :budget_memberships, :budgeted_cost, default: 0
  end
end
