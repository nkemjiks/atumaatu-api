class User < ApplicationRecord
  acts_as_token_authenticatable

  has_many :budgets
  has_many :items
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  validates :firstname, presence: true, length: { minimum: 3 }
  validates :lastname, presence: true, length: { minimum: 3 }
  validates :currency, presence: true

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
end
