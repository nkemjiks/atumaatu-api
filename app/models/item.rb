class Item < ApplicationRecord
  belongs_to :user
  has_many :budget_memberships
  has_many :budgets, through: :budget_memberships

  validates :name, presence: true, uniqueness: { scope: :user_id }
end
