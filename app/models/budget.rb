class Budget < ApplicationRecord
  belongs_to :user
  has_many :budget_memberships, dependent: :destroy
  has_many :items, through: :budget_memberships

  validates :name, presence: true, uniqueness: true
  validates :income, presence: true
end
