class BudgetMembership < ApplicationRecord
  belongs_to :budget
  belongs_to :item

  validates :budgeted_cost, presence: true
  validates_inclusion_of :executed, in: [true, false], on: :update
  validates :actual_cost, presence: true
end
