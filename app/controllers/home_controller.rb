class HomeController < ApplicationController
  skip_before_action :require_login, only: [:index]

  def index
    render :json => "Welcome to atumaatu api. See documentation at https://atumaatu.herokuapp.com/documentation/index.html"
  end
end
