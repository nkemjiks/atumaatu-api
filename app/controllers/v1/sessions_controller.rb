class V1::SessionsController < ApplicationController
  skip_before_action :require_login, only: [:create]

  def create
    begin
      @user = User.find_by!(email: params[:email])
      if @user.valid_password?(params[:password])
        payload = { user_id: @user.id }
        token = encode_token(payload)
        render json: ::UserResource.new(@user, token), status: :created
      else
        raise "Password is incorrect"
      end
    rescue => e
      render json: { error: "User or password is incorrect" }.to_json, status: :unauthorized
    end
  end

  def update
    begin
      @current_user = User.find(params[:id])
      @current_user.update(user_params)
      render json: ::UserResource.new(@current_user, auth_header.split(" ")[1]), status: :ok
    rescue => exception
      render json: { error: exception }, status: :unauthorized
    end
  end

  def destroy
    begin
      @current_user = User.find(params[:id])
      @current_user&.authentication_token = nil
      @current_user.save!
      head(:ok)
    rescue => exception
      render json: { error: exception }, status: :unauthorized
    end
  end

  private

  def user_params
    params.require(:user).permit(:currency)
  end
end
