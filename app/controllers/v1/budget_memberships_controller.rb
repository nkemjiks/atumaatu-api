class V1::BudgetMembershipsController < ApplicationController
  before_action :set_budget, only: [:show, :create, :update, :destroy]

  def show
    if @budget.present?
      render json: BudgetItemsResource.new(@budget, budget_mem_item)
    else
      render json: { errorMessage: "The selected budget doesn't exist" }, status: :unprocessable_entity
    end
  end

  def create
    if @budget.present?
      create_budget_membership
      update_budget_costs
      render json: BudgetItemsResource.new(@budget, budget_mem_item)
    else
      render json: { errorMessage: "The selected budget doesn't exist" }, status: :unprocessable_entity
    end
  end

  def update
    bud_mem = @budget.budget_memberships.where(id: params[:bud_mem_id])
    if bud_mem.present?
      bud_mem.first.update!(
        budgeted_cost: item_params[:budgeted_cost],
        executed: (item_params[:executed] ? 1 : 0),
        actual_cost: item_params[:actual_cost],
      )
      update_budget_costs
      render json: BudgetItemsResource.new(@budget, budget_mem_item)
    else
      render json: { errorMessage: "The selected item doesn't exist in the budget" }, status: :unprocessable_entity
    end
  end

  def destroy
    bud_mem = @budget.budget_memberships.where(id: params[:bud_mem_id])
    if bud_mem.present?
      bud_mem.first.destroy
      update_budget_costs
      render json: BudgetItemsResource.new(@budget, budget_mem_item)
    else
      render json: { errorMessage: "The selected item doesn't exist in the budget" }, status: :unprocessable_entity
    end
  end

  private

  def set_budget
    @budget = session_user.budgets.where(id: params[:id]).first
  end

  def budget_mem_item
    @budget.budget_memberships.includes(:item)
  end

  def update_budget_costs
    budgeted_cost = budget_mem_item.sum(:budgeted_cost)
    actual_cost = budget_mem_item.sum(:actual_cost)
    @budget.update(budgeted_cost: budgeted_cost, actual_cost: actual_cost)
  end

  def item_params
    params.require(:budget_membership).permit(:budgeted_cost, :executed, :actual_cost)
  end

  def create_budget_membership
    create_new_items
    update_budget_membership
  end

  def items_cost
    items_params[:items].map { |item| item[:budgeted_cost] }
  end

  def items_names
    items_params[:items].map { |item| item[:name] }
  end

  def existing_items_name
    existing_items_name ||= session_user.items.where(name: items_names).map(&:name)
  end

  def create_new_items
    (items_names - existing_items_name).each do |item_name|
      session_user.items.create(name: item_name)
    end
  end

  def items_id
    items_id ||= session_user.items.where(name: items_names).map(&:id)
  end

  def update_budget_membership
    items_id.map.with_index do |item_id, i|
      bud_mem = @budget.budget_memberships.find_or_create_by(item_id: item_id)
      bud_mem.update!(budgeted_cost: items_cost[i])
    end
  end

  # Only allow a trusted parameter "white list" through.
  def items_params
    params.permit(:id, items: [:name, :budgeted_cost, :executed, :actual_cost])
  end
end
