class V1::ItemsController < ApplicationController
  before_action :set_item, only: [:show, :update, :destroy]

  def index
    @items = all_items
    render json: ItemsResource.new(@items)
  end

  def create
    @item = session_user.items.new(item_params)

    if @item.save
      render json: ItemsResource.new(all_items)
    else
      render json: { errorMessage: @item.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @item.first.update(item_params)
      render json: ItemsResource.new(all_items)
    else
      render json: { errorMessage: @item.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    bud_mem = BudgetMembership.find_by(item_id: @item_id)
    if @item.present?
      if BudgetMembership.exists?(item_id: @item_id)
        render json: { error: "This item is linked to a budget. Remove from the budget before you delete"}, status: :unprocessable_entity
      else
        @item.first.destroy
        render json: ItemsResource.new(all_items)
      end
    else
      render json: { errorMessage: 'Item does not exist' }, status: :unprocessable_entity
    end
  end

  private
    def set_item
      @item = session_user.items.where(id: params[:id])
    end

    def all_items
      session_user.items
    end
    
    def item_params
      params.require(:item).permit(:name)
    end
end
