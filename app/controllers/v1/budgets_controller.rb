class V1::BudgetsController < ApplicationController
  before_action :set_budget, only: [:show, :update, :destroy]
  skip_before_action :require_login, only: [:create]

  def index
    render json: BudgetsResource.new(user_budgets)
  end

  def create
    @budget = session_user.budgets.new(budget_params)

    if @budget.save
      render json: @budget, status: :created
    else
      render json: @budget.errors, status: :unprocessable_entity
    end
  end

  def copy
    begin
      existing_budget = session_user.budgets.where(id: params[:existing_budget_id]).first
      new_budget = session_user.budgets.create(
        name: "#{existing_budget.name} copy",
        budgeted_cost: existing_budget.budgeted_cost,
        income: existing_budget.income
      )
      new_budget.save!
      existing_budget.budget_memberships.each do |bud_mem|
        new_budget.budget_memberships.create(item_id: bud_mem.item_id, budgeted_cost: bud_mem.budgeted_cost)
      end
      render json: BudgetsResource.new(user_budgets), status: :created
    rescue => exception
      render json: { errorMessage: "An error occured while copying the budget. #{exception}"}, status: :unprocessable_entity
    end
  end

  def update
    if @budget.first.update(budget_params)
      render json: BudgetsResource.new(user_budgets), status: :ok
    else
      render json: { errorMessage: 'An error occured while updating the budget. Check that the name does not exist already'}, status: :unprocessable_entity
    end
  end

  def destroy
    @budget.first.destroy
    @budgets = session_user.budgets
    render json: BudgetsResource.new(@budgets), status: :ok
  end

  private
    def set_budget
      @budget = session_user.budgets.where(id: params[:id])
    end

    def user_budgets
      session_user.budgets.order(created_at: :desc)
    end

    def budget_params
      params.require(:budget).permit(:name, :budgeted_cost, :user_id, :income)
    end
end
