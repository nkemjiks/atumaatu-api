class V1::UsersController < ApplicationController
  skip_before_action :require_login, only: [:create]
  
  def create
    begin
      @user = User.create!(user_params)
      payload = { user_id: @user.id }
      token = encode_token(payload)
      render json: ::UserResource.new(@user, token), status: :created
    rescue => exception
      render json: {error: exception}.to_json, status: :bad_request 
    end
  end

  private

  def user_params
    params.require(:user).permit(:firstname, :lastname, :password, :email, :currency)
  end
end