class UserResource
  def initialize(user, token)
    @user = user
    @token = token
  end

  attr_reader :user, :token

  def as_json(*)
    {
      payload: payload,
      links: links
    }
  end

  private

  def payload
    {
      firstName: user.firstname,
      id: user.id,
      lastName: user.lastname,
      currency: user.currency,
      email: user.email,
      token: token
    }
  end

  def links
    []
  end
end