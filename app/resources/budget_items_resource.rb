class BudgetItemsResource
  def initialize(budget, budget_membership)
    @budget_membership = budget_membership
    @budget = budget
  end

  attr_reader :budget_membership, :budget

  def as_json(*)
    {
      payload: payload,
      links: links
    }
  end

  private

  def payload
    {
      budget: budget,
      items: items
    }
  end

  def items
    budget_membership.map do |bud_mem|
      {
        name: bud_mem.item.name,
        budgetedCost: bud_mem.budgeted_cost,
        actualCost: bud_mem.actual_cost,
        executed: bud_mem.executed,
        bud_mem_id: bud_mem.id,
        item_id: bud_mem.item.id
      }
    end
  end

  def links
    []
  end
end