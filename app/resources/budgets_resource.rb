class BudgetsResource
  def initialize(budgets)
    @budgets = budgets
  end

  attr_reader :budgets

  def as_json(*)
    {
      payload: payload,
      links: links
    }
  end

  private

  def payload
    {
      budgets: budgetItems
    }
  end

  def budgetItems
    budgets.map do |budget|
      {
        name: budget.name,
        budgetedCost: budget.budgeted_cost,
        actualCost: budget.actual_cost,
        id: budget.id,
        income: budget.income,
        createdAt: budget.created_at,
        itemCount: budget.budget_memberships.count,
      }
    end
  end

  def links
    []
  end
end