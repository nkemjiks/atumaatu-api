class ItemsResource
  def initialize(items)
    @items = items
  end

  attr_reader :items

  def as_json(*)
    {
      payload: payload,
      links: links
    }
  end

  private

  def payload
    {
      items: clean_up_item
    }
  end

  def clean_up_item
    items.map do |item|
      {
        name: item.name,
        id: item.id,
        deletable: does_item_have_membership(item.id)
      }
    end
  end

  def does_item_have_membership(id)
    !BudgetMembership.exists?(item_id: id)
  end

  def links
    []
  end
end