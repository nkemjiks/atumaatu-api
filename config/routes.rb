Rails.application.routes.draw do
  # devise_for :users
  root "home#index"
  namespace :v1 do
    resources :sessions, only: [:create, :update, :destroy]
    resources :items, only: [:index, :create, :destroy, :update]
    resources :users, only: [:create]
    resources :budgets, only: [:index, :create, :destroy, :update, :copy] do
      collection do
        put :copy
      end
    end
    resources :budget_memberships, only: [:show, :create, :update, :destroy]
  end
end
